package canva.challenge.com.photomosaic.service;

import android.app.Service;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import canva.challenge.com.photomosaic.task.DissectImageTask;
import canva.challenge.com.photomosaic.task.ReplaceTilesImageTask;
import canva.challenge.com.photomosaic.task.ProcessImageTask;
import canva.challenge.com.photomosaic.task.ProcessTask;
import canva.challenge.com.photomosaic.task.ProcessTaskFactory;


public class ImageService extends Service {

    /**
     * Tag that represents this class
     */
    private static String TAG = ImageService.class.getName();

    /**
     * A class constant that determines the maximum number of threads
     * used to service download requests.
     */
    public static final int MAX_THREADS = 5;

    /**
     * Delay in milliseconds for stopping this service after the last task has been finished
     */

    public static final long DELAY_SERVICE_TIME = 2000;

    /**
     * The ExecutorService that references a ThreadPool.
     */
    private ExecutorService mExecutor;

    private volatile Bitmap mBitmap;


    /**
     *  Handler on UI thread handles incoming messages from the clients and stopself of the service
     */
    private Handler mHandler = new Handler();

    private final Map<ProcessTask, Future> mSubmittedTasks = new HashMap<>();


    public ImageService() {}


    @Override
    public void onCreate() {

        // FixedThreadPool Executor that's configured to use
        // MAX_THREADS. Use a factory method in the Executors class.
        mExecutor = Executors.newFixedThreadPool(MAX_THREADS);

    }


    /**
     * Hook method called when a component calls startService() with
     * the proper Intent.
     */
    @Override
    public int onStartCommand(final Intent intent,
                              final int flags,
                              final int startId) {

        // create a new task depend on given intent
        final ProcessTask task = ProcessTaskFactory.getProcessTask(this, intent);

        if ( task == null ) {
            stopSelf(startId);
            return START_NOT_STICKY;
        }

        // we can do here the logic to interrupt previously processed tasks by cancelling it with future.cancel(true);
        // We can store all the future tasks in into a Map< ProcessTask, Future>


        synchronized (mSubmittedTasks) {

            final ProcessTask runningTask = findTask(task.getClass());



            if ( runningTask!=null ) {
                if ( runningTask instanceof DissectImageTask) {
                    for ( ProcessTask dTask : findAllTask(ReplaceTilesImageTask.class)) {
                        //signal to continue computation
                        ((ReplaceTilesImageTask) dTask).signalToContinue();
                    }
                }

            } else {
                if ( task instanceof ProcessImageTask) {
                    cancelAllTasks();
                }
                mSubmittedTasks.put(task, mExecutor.submit(new HangingServiceRunnable(task, intent, startId)));
            }
        }


        // Tell the Android framework how to behave if this service is
        // interrupted.  In our case, we want to restart the service
        // then re-deliver the intent so that all files are eventually
        // downloaded.
        return START_REDELIVER_INTENT;

    }

    public void addNoHangingTaskToExecutor(ReplaceTilesImageTask partTask, Intent intent) {
        synchronized (mSubmittedTasks) {
            mSubmittedTasks.put(partTask, mExecutor.submit(new NoHangingServiceRunnable(partTask, intent)));
        }
    }


    private class HangingServiceRunnable extends NoHangingServiceRunnable {
        final int startId;

        public HangingServiceRunnable(final ProcessTask task, final Intent intent, final int startId) {
            super(task, intent);
            this.startId = startId;
        }

        @Override
        public void run() {

           super.run();
            // hang a little bit, be ready for the next call
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    stopSelf(startId);
                }
            }, DELAY_SERVICE_TIME);

            Log.d(TAG," ********** " + task.getClass() + " is done ");

        }
    }

    private class NoHangingServiceRunnable implements Runnable {
        final ProcessTask task;
        final Intent intent;


        public NoHangingServiceRunnable(final ProcessTask task, final Intent intent) {
            this.task = task;
            this.intent = intent;
        }

        @Override
        public void run() {

            Log.d(TAG, " ********** " + task.getClass() + " is started ");
            task.onProcess(intent);

            // the task is finished, remove it from the given tasks
            synchronized (mSubmittedTasks) {
                mSubmittedTasks.remove(task);
            }
        }
    }



        private void cancelAllTasks()
    {
        for ( final Map.Entry<ProcessTask, Future> entry : mSubmittedTasks.entrySet()) {
            Future future = entry.getValue();
            try {
                if (future != null && !future.isCancelled() && !future.isDone()) {
                    future.cancel(true);
                }
            } catch ( Exception e ) {
                e.printStackTrace();
            }
        }
        mSubmittedTasks.clear();
    }

    @NonNull
    private List<ProcessTask> findAllTask(Class klass) {

        final List<ProcessTask> tasks = new ArrayList<>();
        // interrupt all the threads
        for (final Map.Entry<ProcessTask, Future> entry : mSubmittedTasks.entrySet()) {

            if (entry.getKey().getClass().isAssignableFrom(klass)) {
                tasks.add( entry.getKey());
            }
        }
        return tasks;
    }

    private ProcessTask findTask(Class klass) {

        final List<ProcessTask> tasks = new ArrayList<>();
        // interrupt all the threads
        for (final Map.Entry<ProcessTask, Future> entry : mSubmittedTasks.entrySet()) {

            if (entry.getKey().getClass().isAssignableFrom(klass)) {
                return entry.getKey();
            }
        }
        return null;
    }

        /**
     * Called when the service is destroyed, which is the last call
     * the Service receives informing it to clean up any resources it
     * holds.
     */
    @Override
        public void onDestroy() {
        // Ensure that the threads used by the ThreadPoolExecutor
        // complete and are reclaimed by the system.

        mExecutor.shutdown();
    }


 /**
     * Return null since this class does not implement a Bound
     * Service.
     */
    @Override
    public IBinder onBind (Intent intent) {
        return null;
    }

    public Bitmap getBitmap() {
        return mBitmap;
    }

    public void setBitmap(Bitmap mBitmap) {
        this.mBitmap = mBitmap;
    }
}
