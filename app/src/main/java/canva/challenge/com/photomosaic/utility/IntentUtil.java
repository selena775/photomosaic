package canva.challenge.com.photomosaic.utility;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

public class IntentUtil {


    public static Intent makeIntent(Context context,
                                    Class<?> service,
                                    String action,
                                    Uri uri, Bundle extras) {

        Intent intent = new Intent(context, service);

        intent.setData(uri);

        if ( extras!= null) {
            intent.putExtras(extras);
        }

        intent.setAction(action);

        return intent;
    }
}
