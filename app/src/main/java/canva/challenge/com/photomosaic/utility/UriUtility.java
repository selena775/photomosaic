package canva.challenge.com.photomosaic.utility;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;

/**
 * Created by sklasnja on 12/16/16.
 */
public class UriUtility {

    public static String getFileName(Context context, Uri uri) {
        String fileName = null;
        String scheme = uri.getScheme();
        if (scheme.equals("file")) {
            fileName = uri.getLastPathSegment();
        }
        else if (scheme.equals("content")) {
            String[] projection = {MediaStore.MediaColumns.DATA};

            ContentResolver cr = context.getContentResolver();
            Cursor metaCursor = cr.query(uri, projection, null, null, null);
            if (metaCursor != null) {
                try {
                    if (metaCursor.moveToFirst()) {
                        fileName = metaCursor.getString(0);
                    }
                } finally {
                    metaCursor.close();
                }
            }
        }
        return fileName;
    }
}
