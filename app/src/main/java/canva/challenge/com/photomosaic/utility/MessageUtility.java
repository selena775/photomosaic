package canva.challenge.com.photomosaic.utility;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;

import static canva.challenge.com.photomosaic.utility.Constants.DETAILED_MESSAGE;
import static canva.challenge.com.photomosaic.utility.Constants.IMAGE_BITMAP;
import static canva.challenge.com.photomosaic.utility.Constants.TASK_RESULT;

public class MessageUtility {

    public static final String BUNDLE_EXTRA = "BUNDLE_EXTRA";

    public static void sendBroadcastMessage( final Context context, final String intentAction, final Bundle bundle) {
        LocalBroadcastManager mBroadcastMgr = LocalBroadcastManager.getInstance(context);

        Intent intent= new Intent(intentAction);
        intent.putExtra(BUNDLE_EXTRA, bundle);
        mBroadcastMgr.sendBroadcast(intent);
    }


    public static void sendBroadcastMessage( final Context context, final String broadcastAction,
                                             final String result, final String message, final Bitmap bitmap) {
        final Bundle resultBundle = new Bundle();
        resultBundle.putString(TASK_RESULT,result);
        resultBundle.putString(DETAILED_MESSAGE, message);
        resultBundle.putParcelable(IMAGE_BITMAP, bitmap);
        sendBroadcastMessage(context, broadcastAction, resultBundle);
    }
}
