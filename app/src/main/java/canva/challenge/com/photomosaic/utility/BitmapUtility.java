package canva.challenge.com.photomosaic.utility;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

/**
 * Created by sklasnja on 12/16/16.
 */
public class BitmapUtility {

    private static int sImageMaxSize;

    static {
        // Get max available VM memory, exceeding this amount will throw an
        // OutOfMemory exception.
        final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 512);

        if ( maxMemory <= 65536) {
            sImageMaxSize = 300;
        } else {
            sImageMaxSize = 512;
        }
    }


    public static Bitmap decodeSampledBitmap(File f) throws IOException {

        //Decode image size
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;

        FileInputStream fis = new FileInputStream(f);
        BitmapFactory.decodeStream(fis, null, options);
        fis.close();

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options);
        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;

        fis = new FileInputStream(f);
        Bitmap b = BitmapFactory.decodeStream(fis, null, options);
        fis.close();

        return b;
    }

    private static int calculateInSampleSize( BitmapFactory.Options options) {

        int inSampleSize = 1;
        if (options.outHeight > sImageMaxSize || options.outWidth > sImageMaxSize) {
            inSampleSize = (int)Math.pow(2, (int) Math.ceil(Math.log(sImageMaxSize /
                    (double) Math.max(options.outHeight, options.outWidth)) / Math.log(0.5)));
        }
        return inSampleSize;
    }

}
