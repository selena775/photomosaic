package canva.challenge.com.photomosaic.utility;

/**
 * Created by sklasnja on 12/16/16.
 */
public class Constants {

    public static final String LOAD_BITMAP = "canva.challenge.com.photomosaic.service.ImageService.LOAD_BITMAP";
    public static final String DISECT_BITMAP = "canva.challenge.com.photomosaic.service.ImageService.DISECT_BITMAP";

    public static class Result {
        public static final String OK = "OK";
        public static final String FAILED = "FAILED";
    }

    public static final String IMAGE_BITMAP = "canva.challenge.com.photomosaic.task.ProcessImageTask.IMAGE_BITMAP";
    public static final String TASK_RESULT= "canva.challenge.com.photomosaic.utility.TASK_RESULT";
    public static final String DETAILED_MESSAGE= "canva.challenge.com.photomosaic.utility.DETAILED_MESSAGE";

    public static final String BROADCAST_ACTION_INITIAL_BITMAP = "canva.challenge.com.photomosaic.task.ProcessImageTask.BROADCAST_ACTION_INITIAL_BITMAP";
    public static final String BROADCAST_ACTION_BITMAP_DISSECTED = "canva.challenge.com.photomosaic.task.ReplaceTilesImageTask.BROADCAST_ACTION_BITMAP_DISSECTED";
    public static final String BROADCAST_ACTION_BITMAP_DISSECTED_DONE = "canva.challenge.com.photomosaic.task.ReplaceTilesImageTask.BROADCAST_ACTION_BITMAP_DISSECTED_DONE";


    public static final String TILE_WIDTH = "canva.challenge.com.photomosaic.task.ReplaceTilesImageTask.TILE_WIDTH";
    public static final String TILE_HEIGHT = "canva.challenge.com.photomosaic.task.ReplaceTilesImageTask.TILE_HEIGHT";
    public static final String SERVER = "canva.challenge.com.photomosaic.task.ReplaceTilesImageTask.SERVER";

    public static int DEFAULT_TILE_WIDTH = 32;
    public static int DEFAULT_TILE_HEIGHT = 32;
    public static String DEFAULT_SERVER = "196.252.183.5";
}
