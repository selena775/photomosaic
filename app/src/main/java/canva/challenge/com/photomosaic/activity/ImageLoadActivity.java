package canva.challenge.com.photomosaic.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import canva.challenge.com.photomosaic.R;
import canva.challenge.com.photomosaic.task.ReplaceTilesImageTask;
import canva.challenge.com.photomosaic.task.ProcessImageTask;
import canva.challenge.com.photomosaic.utility.Constants;
import canva.challenge.com.photomosaic.utility.MessageUtility;
import canva.challenge.com.photomosaic.utility.NetworkUtil;

import static canva.challenge.com.photomosaic.utility.Constants.BROADCAST_ACTION_BITMAP_DISSECTED;
import static canva.challenge.com.photomosaic.utility.Constants.BROADCAST_ACTION_BITMAP_DISSECTED_DONE;
import static canva.challenge.com.photomosaic.utility.Constants.BROADCAST_ACTION_INITIAL_BITMAP;
import static canva.challenge.com.photomosaic.utility.Constants.DEFAULT_SERVER;
import static canva.challenge.com.photomosaic.utility.Constants.DEFAULT_TILE_HEIGHT;
import static canva.challenge.com.photomosaic.utility.Constants.DEFAULT_TILE_WIDTH;
import static canva.challenge.com.photomosaic.utility.Constants.DETAILED_MESSAGE;
import static canva.challenge.com.photomosaic.utility.Constants.Result;
import static canva.challenge.com.photomosaic.utility.Constants.TASK_RESULT;

public class ImageLoadActivity extends AppCompatActivity {

    public static String TAG = ImageLoadActivity.class.getName();

    private static IntentFilter sIntentFilter = new IntentFilter();

    public static String CURRENT_BITMAP = ImageLoadActivity.class + ".CURRENT_BITMAP";
    public static String PROCESS_DONE = ImageLoadActivity.class + ".PROCESS_DONE";
    public static String LAST_SEEN_URI = ImageLoadActivity.class + ".LAST_SEEN_URI";
    public static String LAST_SET_TILE_WIDTH = ImageLoadActivity.class + ".LAST_SET_TILE_WIDTH";
    public static String LAST_SET_TILE_HEIGHT = ImageLoadActivity.class + ".LAST_SET_TILE_HEIGHT";
    public static String LAST_SEEN_SERVER = ImageLoadActivity.class + ".LAST_SEEN_SERVER";

    private static int PICK_FILE_REQUEST_CODE = 10001;

    private BroadcastReceiver mBroadcastReceiver;
    private BroadcastReceiver mNetworkReceiver;

    private Bitmap mCurrentBitmap;

    private boolean mProcessDone;

    private Uri mLastSeenUri;

    static {
        sIntentFilter.addAction(Constants.BROADCAST_ACTION_INITIAL_BITMAP);
        sIntentFilter.addAction(Constants.BROADCAST_ACTION_BITMAP_DISSECTED);
        sIntentFilter.addAction(Constants.BROADCAST_ACTION_BITMAP_DISSECTED_DONE);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_image_load);

        restoreInstanceState(savedInstanceState);

        setEditDefaults(savedInstanceState);

        setImageViewBitmap();

        setScaleTypeSpinner();

        hideSoftKeyboard();

    }

    private void setEditDefaults(final Bundle savedInstanceState) {

        final String server;
        final int tileWidth;
        final int tileHeight;
        if (savedInstanceState != null) {
            server = savedInstanceState.getString(LAST_SEEN_SERVER, DEFAULT_SERVER);
            tileWidth = savedInstanceState.getInt(LAST_SET_TILE_WIDTH, DEFAULT_TILE_WIDTH);
            tileHeight = savedInstanceState.getInt(LAST_SET_TILE_HEIGHT, DEFAULT_TILE_HEIGHT);
        } else {
            server = DEFAULT_SERVER;
            tileWidth = DEFAULT_TILE_WIDTH;
            tileHeight = DEFAULT_TILE_HEIGHT;
        }


        final EditText serverEdit = (EditText)findViewById(R.id.server);
        if ( serverEdit!=null ) {
            serverEdit.setText(server);
        }

        final EditText tileHeightEdit = (EditText)findViewById(R.id.tile_height);
        if ( tileHeightEdit!=null ) tileHeightEdit.setText(String.valueOf(tileWidth));


        final EditText tileWidthEdit = (EditText)findViewById(R.id.tile_width);
        if ( tileWidthEdit!=null ) tileWidthEdit.setText(String.valueOf(tileHeight));
    }

    private void hideSoftKeyboard() {
        // Check if no view has focus:
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    private void setImageViewBitmap() {
        final ImageView imageView = (ImageView)findViewById(R.id.imageView);
        if ( imageView != null ) {
            imageView.setImageBitmap(mCurrentBitmap);
        }
    }

    private void setScaleTypeSpinner() {

        final Spinner spinner = (Spinner) findViewById(R.id.spinner);
        if ( spinner!=null ) {
            // Create an ArrayAdapter using the string array and a default spinner layout
            ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                    R.array.scaleType, android.R.layout.simple_spinner_item);
            // Specify the layout to use when the list of choices appears
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            // Apply the adapter to the spinner
            spinner.setAdapter(adapter);

            spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(final AdapterView<?> parent, final View view, final int position, final long id) {

                    final ImageView imageView = (ImageView)findViewById(R.id.imageView);

                    if (imageView != null) {
                        String scaleType = (String) parent.getItemAtPosition(position);
                        imageView.setScaleType(translate(scaleType));
                    }
                    hideSoftKeyboard();
                }

                @Override
                public void onNothingSelected(final AdapterView<?> parent) {
                    hideSoftKeyboard();
                }
            });
            int spinnerPosition = adapter.getPosition("center");

            //set the default according to value
            spinner.setSelection(spinnerPosition);
        }
    }

    private void restoreInstanceState(final Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            // Restore value of members from saved state
            mCurrentBitmap = (Bitmap)savedInstanceState.get(CURRENT_BITMAP);

            mProcessDone = savedInstanceState.getBoolean(PROCESS_DONE, false);

            mLastSeenUri = savedInstanceState.getParcelable(LAST_SEEN_URI);
        }
    }

    private ImageView.ScaleType translate(final String scaleType) {

        final ImageView.ScaleType r;

        switch (scaleType) {

            case "fitXY":
                r = ImageView.ScaleType.FIT_XY;
                break;
            case "fitStart":
                r = ImageView.ScaleType.FIT_START;
                break;
            case "fitCenter":
                r = ImageView.ScaleType.FIT_CENTER;
                break;
            case "fitEnd":
                r = ImageView.ScaleType.FIT_END;
                break;
            case "center":
                r = ImageView.ScaleType.CENTER;
                break;
            case "centerCrop":
                r = ImageView.ScaleType.CENTER_CROP;
                break;
            case "centerInside":
                r = ImageView.ScaleType.CENTER_INSIDE;
                break;
            default:
                r = ImageView.ScaleType.FIT_XY;
                break;
        }
        return r;
    }

    public void onImageLoad(View view) {

        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType("image/*");
        startActivityForResult(intent, PICK_FILE_REQUEST_CODE);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Check which request we're responding to
        if (requestCode == PICK_FILE_REQUEST_CODE) {
            // Make sure the request was successful
            if (resultCode == RESULT_OK) {
                // The user picked a contact.
                // The Intent's data Uri identifies which contact was selected.

                // reset the state for the newly selected image

                resetStates();

                mLastSeenUri = data.getData();

                startServiceForImageLoad();
                hideSoftKeyboard();
            } else {
                Toast.makeText(ImageLoadActivity.this, "Failed to select file, please check the given permissions", Toast.LENGTH_LONG).show();
            }
        }
    }

    private void startServiceForImageLoad() {

        if ( mLastSeenUri == null) return;

        ProcessImageTask.startServiceTask(this, mLastSeenUri, getPreferredServer(), getPreferredTileWidth(), getPreferredTileHeight());

    }

    public String getPreferredServer() {
        final EditText server = (EditText)findViewById(R.id.server);
        final Editable editable;
        String text;
        if ( server != null && (editable=server.getText())!=null && !(text= editable.toString()).isEmpty())
        {
            return text;
        }
        return  DEFAULT_SERVER;
    }

    private int getPreferredTileWidth(){

        final EditText tileWidthEdit = (EditText)findViewById(R.id.tile_width);
        final Editable editable;
        if ( tileWidthEdit != null && (editable=tileWidthEdit.getText())!=null)
        {
            try {
                return Integer.valueOf(editable.toString());
            } catch (Exception ex){
                // do nothing
            }

        }
        return  DEFAULT_TILE_WIDTH;
    }

    private int getPreferredTileHeight(){

        final EditText tileHeightEdit = (EditText)findViewById(R.id.tile_height);
        final Editable editable;
        if ( tileHeightEdit != null && (editable=tileHeightEdit.getText())!=null)
        {
            try {
                return Integer.valueOf(editable.toString());
            } catch (Exception ex){
                // do nothing
            }

        }
        return  DEFAULT_TILE_HEIGHT;
    }


    public void startServiceForImageLoad(View view) {
        mCurrentBitmap = null;
        mProcessDone = false;
        startServiceForImageLoad();
        hideSoftKeyboard();
    }


    private void resetStates() {
        mLastSeenUri = null;
        mProcessDone = false;
        mCurrentBitmap = null;

    }


    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        // Save the user's current game state
        savedInstanceState.putParcelable(CURRENT_BITMAP, mCurrentBitmap);
        savedInstanceState.putBoolean(PROCESS_DONE, mProcessDone);
        savedInstanceState.putParcelable(LAST_SEEN_URI, mLastSeenUri);
        savedInstanceState.putString(LAST_SEEN_SERVER, getPreferredServer());
        savedInstanceState.putInt(LAST_SET_TILE_WIDTH, getPreferredTileWidth());
        savedInstanceState.putInt(LAST_SET_TILE_HEIGHT, getPreferredTileHeight());

        // Always call the superclass so it can save the view hierarchy state
        super.onSaveInstanceState(savedInstanceState);
    }



    @Override
    protected void onStart() {

        super.onStart();

        if ( mBroadcastReceiver==null) {
            mBroadcastReceiver = new ImageReceiver();
            LocalBroadcastManager.getInstance(getApplicationContext()).registerReceiver(mBroadcastReceiver, sIntentFilter);
        }
        if ( mNetworkReceiver==null) {
            mNetworkReceiver = new NetworkChangeReceiver();

            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction("android.net.ConnectivityManager.CONNECTIVITY_ACTION");
            intentFilter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
            registerReceiver(mNetworkReceiver, intentFilter);
        }
    }


    @Override
    protected void onStop() {

        if ( mBroadcastReceiver!=null) {
            LocalBroadcastManager.getInstance(getApplicationContext()).unregisterReceiver(mBroadcastReceiver);
            mBroadcastReceiver = null;
        }
        if ( mNetworkReceiver!=null) {
            unregisterReceiver(mNetworkReceiver);
            mNetworkReceiver = null;
        }

        super.onStop();
    }


    private class ImageReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(final Context context, final Intent intent) {
            final String action = intent.getAction();
            final Bundle bundle = intent.getParcelableExtra(MessageUtility.BUNDLE_EXTRA);
            if ( bundle == null ) return;

            String result = bundle.getString(TASK_RESULT);
            String detailedMessage = bundle.getString(DETAILED_MESSAGE);

            if( detailedMessage!=null ) {
                Log.d(TAG, detailedMessage);
            }

            // if it is an error return
            // toast the error message if any
            if(  !Result.OK.equals(result)) {
                if ( detailedMessage!=null)
                    Toast.makeText(ImageLoadActivity.this, detailedMessage, Toast.LENGTH_LONG).show();
                return;
            }

            switch (action) {

                case BROADCAST_ACTION_INITIAL_BITMAP:
                case BROADCAST_ACTION_BITMAP_DISSECTED:

                    final Bitmap bitmap = bundle.getParcelable(Constants.IMAGE_BITMAP);
                    if (bitmap == null) break;

                    final ImageView view = (ImageView) findViewById(R.id.imageView);
                    if (view != null) {
                        view.setImageBitmap(bitmap);
                        mCurrentBitmap = bitmap;
                    }

                    break;
                case BROADCAST_ACTION_BITMAP_DISSECTED_DONE:
                    mProcessDone = true;
                    break;

                default:
                    break;
            }

        }
    }

    public class NetworkChangeReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(final Context context, final Intent intent) {

           if (NetworkUtil.hasInternetConnection(context) && mCurrentBitmap!=null && !mProcessDone) {
               ReplaceTilesImageTask.startServiceTask(ImageLoadActivity.this,
                       getPreferredServer(), getPreferredTileWidth(), getPreferredTileHeight());
           }
        }
    }
}
