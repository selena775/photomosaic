package canva.challenge.com.photomosaic.task;

import android.content.Intent;

import canva.challenge.com.photomosaic.service.ImageService;

import static canva.challenge.com.photomosaic.utility.Constants.*;

/**
 * Created by sklasnja on 12/16/16.
 */
public class ProcessTaskFactory {

    public static ProcessTask getProcessTask(final ImageService service, final Intent intent) {

        switch (intent.getAction()){
            case LOAD_BITMAP:
                return new ProcessImageTask(service);
            case DISECT_BITMAP:
                return new DissectImageTask(service);
            default:
                break;
        }
        return null;
    }
}
