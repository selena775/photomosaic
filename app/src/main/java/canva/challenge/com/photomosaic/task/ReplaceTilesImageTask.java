package canva.challenge.com.photomosaic.task;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.io.InterruptedIOException;
import java.net.ConnectException;
import java.net.HttpURLConnection;
import java.net.SocketException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

import canva.challenge.com.photomosaic.service.ImageService;
import canva.challenge.com.photomosaic.utility.AverageColorUtility;
import canva.challenge.com.photomosaic.utility.IntentUtil;
import canva.challenge.com.photomosaic.utility.NetworkUtil;

import static canva.challenge.com.photomosaic.utility.Constants.DEFAULT_SERVER;
import static canva.challenge.com.photomosaic.utility.Constants.DEFAULT_TILE_HEIGHT;
import static canva.challenge.com.photomosaic.utility.Constants.DEFAULT_TILE_WIDTH;
import static canva.challenge.com.photomosaic.utility.Constants.DISECT_BITMAP;
import static canva.challenge.com.photomosaic.utility.Constants.SERVER;
import static canva.challenge.com.photomosaic.utility.Constants.TILE_HEIGHT;
import static canva.challenge.com.photomosaic.utility.Constants.TILE_WIDTH;
import static canva.challenge.com.photomosaic.utility.MessageUtility.sendBroadcastMessage;


public class ReplaceTilesImageTask implements ProcessTask {

    CountDownLatch mCountDownLatch;
    Bitmap composedRow;
    int row;
    int to;
    int from;
    int offsetX;


    private String mException = "";

    /**
     * Tag that represents this class
     */
    public static String TAG = ReplaceTilesImageTask.class.getName();

    private Context mContext;

       private final ReentrantLock mLock = new ReentrantLock();
    private final Condition mNotNetwork = mLock.newCondition();


    public ReplaceTilesImageTask(final Context context) {
        setContext( context);
    }

    @Override
    public void onProcess(final Intent intent) {

        final Bundle bundle= intent.getExtras();
        try {

            int tileWidth = bundle.getInt(TILE_WIDTH, DEFAULT_TILE_WIDTH);
            int tileHeight = bundle.getInt(TILE_HEIGHT, DEFAULT_TILE_HEIGHT);
            String server = bundle.getString(SERVER, DEFAULT_SERVER);
            boolean firstWarning = true;


            boolean noNetwork = false;


            Canvas canvasRow = new Canvas(composedRow);


            for (int j = from; j < to; j++) {

                do {
                    HttpURLConnection connection = null;
                    try {

                        mException = "";
                        noNetwork = false;

                        final Bitmap originalTile = Bitmap.createBitmap(composedRow, offsetX + j * tileWidth, 0, tileWidth, tileHeight);

                        // gracefully exist in case the thread is cancelled
                        if (Thread.currentThread().isInterrupted()) return;

                        // calculate average color
                        // for now let's assume white is background color,later we might include this in the calculation
                        String dominantColour = AverageColorUtility.getAverageColorRgb(originalTile, Color.WHITE);

                        StringBuilder sb = new StringBuilder("http://");
                        sb.append(server).append(":8765/color/");
                        sb.append(tileWidth).append('/').append(tileHeight).append('/').append(dominantColour);

                        java.net.URL url = new java.net.URL(sb.toString());
                        connection = (HttpURLConnection) url.openConnection();

                        connection.setDoInput(true);

                        connection.connect();
                        InputStream in = new BufferedInputStream(connection.getInputStream());

                        Bitmap newTile = BitmapFactory.decodeStream(in);
                        canvasRow.drawBitmap(newTile, offsetX + j * tileWidth, 0, null);

                        mException = null;

                    } catch (ConnectException | NullPointerException ex) {
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException e) {
                            return;
                        }
                        if (NetworkUtil.hasInternetConnection(getContext())) {
                            // we have network available but something is wrong with the server
                            mException = "Encountering problems with the server settings";
                        } else {
                            if (firstWarning) {
                                // we don't have connection, here notify the user first time only                        }
                                mException = "Encountering problems with connectivity";
                                firstWarning = false;
                            }
                            noNetwork = true;
                            Log.w(TAG, "----------- Network problem");
                        }


                    } catch (SocketException e) {

                        // TODO something happened with the server, maybe to try several more times
                        // but for now stop trying
                        mException = "Encountering problems with the server communication";

                    } catch (InterruptedIOException e) {

                        return;
                        // do nothing

                    } catch (Exception e) {

                        if (e instanceof InterruptedException) return;

                        // unknown exception
                        e.printStackTrace();

                    } finally {
                        if ( connection!=null ) {
                            connection.disconnect();
                        }
                    }

                    try {

                        if (noNetwork) {
                            mLock.lockInterruptibly();
                            try {
                                while (!NetworkUtil.hasInternetConnection(getContext())) {
                                    // wait here until network is reestablished
                                    mNotNetwork.await();
                                }
                            } finally {
                                mLock.unlock();
                            }
                        }
                    } catch (InterruptedException e) {
                        return;
                    }

                } while (noNetwork);
            }
        } finally {
            mCountDownLatch.countDown();
        }
    }

    public String getException() {
        return mException;
    }

    public Context getContext() {
        return mContext;
    }

    public void setContext(final Context context) {
        mContext = context;
    }

    public static void startServiceTask(final Context context, final String server, final int tileWidth, final int tileHeight) {
        final Bundle bundle= new Bundle();
        bundle.putInt(TILE_WIDTH, tileWidth);
        bundle.putInt(TILE_HEIGHT, tileHeight);
        bundle.putString(SERVER, server);

        context.startService(IntentUtil.makeIntent(context, ImageService.class,
                DISECT_BITMAP, null, bundle));
    }

    public void signalToContinue(){
        mLock.lock();
        try {
            mNotNetwork.signalAll();
        } finally {
            mLock.unlock();
        }
    }


    public void setCountDownLatch(CountDownLatch countDownLatch) {
        mCountDownLatch = countDownLatch;
    }

    public void setComposedRow(Bitmap composedRow) {
        this.composedRow = composedRow;
    }

    public void setRow(int row) {
        this.row = row;
    }

    public void setTo(int to) {
        this.to = to;
    }

    public void setFrom(int from) {
        this.from = from;
    }

    public void setOffsetX(int offsetX) {
        this.offsetX = offsetX;
    }
}
