package canva.challenge.com.photomosaic.task;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;

import java.io.File;

import canva.challenge.com.photomosaic.service.ImageService;
import canva.challenge.com.photomosaic.utility.IntentUtil;
import canva.challenge.com.photomosaic.utility.UriUtility;

import static canva.challenge.com.photomosaic.utility.BitmapUtility.decodeSampledBitmap;
import static canva.challenge.com.photomosaic.utility.Constants.BROADCAST_ACTION_INITIAL_BITMAP;
import static canva.challenge.com.photomosaic.utility.Constants.DEFAULT_SERVER;
import static canva.challenge.com.photomosaic.utility.Constants.DEFAULT_TILE_HEIGHT;
import static canva.challenge.com.photomosaic.utility.Constants.DEFAULT_TILE_WIDTH;
import static canva.challenge.com.photomosaic.utility.Constants.LOAD_BITMAP;
import static canva.challenge.com.photomosaic.utility.Constants.Result;
import static canva.challenge.com.photomosaic.utility.Constants.SERVER;
import static canva.challenge.com.photomosaic.utility.Constants.TILE_HEIGHT;
import static canva.challenge.com.photomosaic.utility.Constants.TILE_WIDTH;
import static canva.challenge.com.photomosaic.utility.MessageUtility.sendBroadcastMessage;

public class ProcessImageTask implements ProcessTask {

    /**
     * Tag that represents this class
     */
    public static String TAG = ProcessImageTask.class.getName();

    private ImageService mImageService;

    private Bitmap mBitmap;


    public ProcessImageTask(){}

    public ProcessImageTask(final ImageService imageService) {
        setService( imageService);
    }

    @Override
    public void onProcess(final Intent intent) {

        Uri uri = intent.getData();
        Bundle bundle= intent.getExtras();

        try {
            String fileName = UriUtility.getFileName(getContext(), uri);

            if (fileName == null)
            {
                sendBroadcastMessage(getContext(), BROADCAST_ACTION_INITIAL_BITMAP,
                        Result.FAILED, "No file to process", null);

            } else {

                // load the bitmap respecting the device capabilities
                mBitmap = decodeSampledBitmap(new File(fileName));

                // gracefully exist in case the thread is cancelled
                if ( Thread.currentThread().isInterrupted() ) return;

                // send a broadcast message with the bitmap in the message
                sendBroadcastMessage(getContext(), BROADCAST_ACTION_INITIAL_BITMAP,
                        Result.OK, "Loading Bitmap completed", mBitmap);

                postProcess(bundle);

            }
        } catch (final Exception e) {
            e.printStackTrace();
            sendBroadcastMessage(getContext(), BROADCAST_ACTION_INITIAL_BITMAP,
                    Result.FAILED, e.getMessage(), null);
        }
    }


    public void postProcess(final Bundle bundle) {
        if ( mBitmap!=null) {
            mImageService.setBitmap(mBitmap);
            ReplaceTilesImageTask.startServiceTask(getContext(),
                    bundle.getString(SERVER, DEFAULT_SERVER),
                    bundle.getInt(TILE_WIDTH, DEFAULT_TILE_WIDTH),
                    bundle.getInt(TILE_HEIGHT, DEFAULT_TILE_HEIGHT));
        }
    }

    @Override
    public String getException() {
        return null;
    }

    public static void startServiceTask(final Context context, final Uri uri, final String server, final int tileWidth, final int tileHeight) {

        final Bundle bundle= new Bundle();
        bundle.putInt(TILE_WIDTH, tileWidth);
        bundle.putInt(TILE_HEIGHT, tileHeight);
        bundle.putString(SERVER, server);

        context.startService(IntentUtil.makeIntent(context, ImageService.class,
                LOAD_BITMAP, uri, bundle));
    }


    public Context getContext() {
        return mImageService;
    }

    public void setService(final ImageService imageService) {
        mImageService = imageService;
    }
}
