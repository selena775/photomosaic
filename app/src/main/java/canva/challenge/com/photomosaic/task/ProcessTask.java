package canva.challenge.com.photomosaic.task;

import android.content.Intent;

public interface ProcessTask {
    void onProcess(final Intent intent);

    String getException();
}
