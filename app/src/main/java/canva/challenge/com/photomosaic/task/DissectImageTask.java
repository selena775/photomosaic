package canva.challenge.com.photomosaic.task;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.os.Bundle;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;

import canva.challenge.com.photomosaic.service.ImageService;
import canva.challenge.com.photomosaic.utility.Constants;

import static canva.challenge.com.photomosaic.utility.Constants.BROADCAST_ACTION_BITMAP_DISSECTED;
import static canva.challenge.com.photomosaic.utility.Constants.BROADCAST_ACTION_BITMAP_DISSECTED_DONE;
import static canva.challenge.com.photomosaic.utility.Constants.DEFAULT_TILE_HEIGHT;
import static canva.challenge.com.photomosaic.utility.Constants.DEFAULT_TILE_WIDTH;
import static canva.challenge.com.photomosaic.utility.Constants.Result.OK;
import static canva.challenge.com.photomosaic.utility.Constants.TILE_HEIGHT;
import static canva.challenge.com.photomosaic.utility.Constants.TILE_WIDTH;
import static canva.challenge.com.photomosaic.utility.MessageUtility.sendBroadcastMessage;
import static canva.challenge.com.photomosaic.service.ImageService.MAX_THREADS;

/**
 * Created by sklasnja on 2/01/17.
 */
public class DissectImageTask implements ProcessTask {


    public static String TAG = DissectImageTask.class.getName();


    private ImageService imageService;

    public DissectImageTask(){}

    public DissectImageTask(final ImageService imageService) {
        setService( imageService);
    }

    @Override
    public void onProcess(final Intent intent) {

        final Bundle bundle= intent.getExtras();

        final Bitmap bitmap = imageService.getBitmap();
        if (bitmap == null) {
            sendBroadcastMessage(getContext(), BROADCAST_ACTION_BITMAP_DISSECTED,
                    Constants.Result.FAILED, "No bitmap passed to the service", null);
            return ;
        }

        int tileWidth = bundle.getInt(TILE_WIDTH, DEFAULT_TILE_WIDTH);
        int tileHeight = bundle.getInt(TILE_HEIGHT, DEFAULT_TILE_HEIGHT);


        Bitmap composedBitmap = bitmap.copy(Bitmap.Config.ARGB_8888, true);
        Canvas canvasComposed = new Canvas(composedBitmap);
        int numRows= bitmap.getHeight() / tileHeight;
        int numCols = bitmap.getWidth() / tileWidth;
        int offsetX = (bitmap.getWidth() - numCols * tileWidth) / 2;
        int offsetY = (bitmap.getHeight() - numRows * tileHeight) / 2;

        int threadNum = MAX_THREADS - 1;
        int numCells = numCols / threadNum;


        for ( int i= 0; i < numRows; i++ ) {

            final CountDownLatch countDownLatch = new CountDownLatch(threadNum);
            final Bitmap composedRow = Bitmap.createBitmap(bitmap, 0, offsetY + i * tileHeight, bitmap.getWidth(), tileHeight);

            int col = 0;
            int rest = numCols - numCells * threadNum;


            final List<ReplaceTilesImageTask> paralelTasks = new ArrayList<>(threadNum);

            // here do paralelisam
            for ( int thread = 0; thread <  threadNum; thread++ ){

                ReplaceTilesImageTask partTask = new ReplaceTilesImageTask(getContext());
                partTask.setCountDownLatch(countDownLatch);
                partTask.setComposedRow(composedRow);
                partTask.setRow(i);
                partTask.setOffsetX(offsetX);

                partTask.setFrom(col);
                col+= numCells;
                if( rest > 0) {
                    col++;
                    rest--;
                }
                partTask.setTo(col);


                paralelTasks.add(partTask);
                imageService.addNoHangingTaskToExecutor(partTask, intent);

            }

            try{

                countDownLatch.await();  //main thread is waiting on CountDownLatch to finish

                for ( ReplaceTilesImageTask replaceTilesTask : paralelTasks) {


                    String exception = replaceTilesTask.getException();
                    if ( exception != null ) {
                        if (!exception.isEmpty()) {
                            // send the exception message
                            sendBroadcastMessage(getContext(), BROADCAST_ACTION_BITMAP_DISSECTED, Constants.Result.FAILED,
                                    exception, composedBitmap);

                        }
                        break;
                    }
                }

                // since there was exception, copy the row into the composed bitmap
                canvasComposed.drawBitmap(composedRow, 0, offsetY + i * tileHeight, null);

                // no exception here, send the changed bitmap
                sendBroadcastMessage(getContext(), BROADCAST_ACTION_BITMAP_DISSECTED, Constants.Result.OK,
                        MessageFormat.format("Finished processing for the row: {0} out of {1}", i + 1, numRows), composedBitmap);


            } catch(InterruptedException ie){
                return;
            }

        }
        sendBroadcastMessage(getContext(), BROADCAST_ACTION_BITMAP_DISSECTED_DONE, OK, "All done", null);
    }


    @Override
    public String getException() {
        return null;
    }


    public Context getContext() {
        return imageService.getApplicationContext();
    }

    public void setService(final ImageService imageService) {
        this.imageService = imageService;
    }
}
