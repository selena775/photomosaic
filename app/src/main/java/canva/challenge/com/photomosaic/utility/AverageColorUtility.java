package canva.challenge.com.photomosaic.utility;

import android.graphics.Bitmap;
import android.graphics.Color;

/**
 * Created by sklasnja on 12/16/16.
 */
public class AverageColorUtility {

    public static int getAverageColor(Bitmap bitmap, int background) {


        int backgroundR = Color.red(background);
        int backgroundG = Color.green(background);
        int backgroundB = Color.blue(background);

        long redBucket = 0;
        long greenBucket = 0;
        long blueBucket = 0;
        long pixelCount = 0;

        for ( int y = 0; y < bitmap.getHeight(); y++) {
            for (int x = 0; x < bitmap.getWidth(); x++) {
                int c = bitmap.getPixel(x, y);

                int alpha = Color.alpha(c);

                redBucket += transparentColor( Color.red(c), backgroundR, alpha);
                greenBucket += transparentColor( Color.green(c), backgroundG, alpha);
                blueBucket += transparentColor( Color.blue(c), backgroundB, alpha);
                pixelCount++;

            }
        }

        return Color.rgb(
                (int) (redBucket / pixelCount),
                (int) (greenBucket / pixelCount),
                (int) (blueBucket / pixelCount));

    }


    public static int transparentColor(int intFgColor, int intBgColor, int intAlpha)
    {
        double dAlpha = intAlpha/255.0;
        double dFirst = intFgColor * dAlpha;
        double dSecond = intBgColor * (1 - dAlpha);
        return (int)(dFirst + dSecond);
    }

    public static String getAverageColorRgb(Bitmap bitmap, int background) {
        int c= getAverageColor(bitmap, background);

        return String.format("%08X", c).substring(2);
    }



}
