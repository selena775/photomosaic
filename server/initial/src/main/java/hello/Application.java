package hello;

import java.util.Arrays;
import java.util.HashMap;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ApplicationContext;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@SpringBootApplication
@EnableAutoConfiguration
@EnableWebMvc
public class Application {
    
    public static void main(String[] args) {

        HashMap<String, Object> props = new HashMap<>();
        props.put("server.port", 8765);

        new SpringApplicationBuilder()
                .sources(Application.class)
                .properties(props)
                .run(args);


    }

}
