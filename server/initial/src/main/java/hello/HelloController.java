package hello;

import com.google.common.io.ByteStreams;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.client.HttpClientErrorException;

import javax.imageio.ImageIO;
import javax.imageio.stream.ImageOutputStream;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

@Controller
public class HelloController {
    
    @RequestMapping("/")
    public String index() {
        return "Greetings from Spring Boot!";
    }


    @RequestMapping("/color/{width}/{height}/{color}")
    public ResponseEntity<byte[]> getPhotoOfColor (@PathVariable Integer width, @PathVariable Integer height, @PathVariable String color) {

        try {

            // TYPE_INT_ARGB specifies the image format: 8-bit RGBA packed
            // into integer pixels
            BufferedImage bi = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);

            Graphics2D ig2 = bi.createGraphics();
            ig2.setPaint(Color.decode("#" + color));
            ig2.fillOval(0,0,width,height);

            final HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.IMAGE_PNG);

            ByteArrayOutputStream os = new ByteArrayOutputStream();
            ImageIO.write(bi,"png", os);
            InputStream in = new ByteArrayInputStream(os.toByteArray());

            return new ResponseEntity<byte[]>(ByteStreams.toByteArray(in), headers, HttpStatus.CREATED);

        } catch ( IOException ex) {

        }
        throw new HttpClientErrorException(HttpStatus.NOT_FOUND);

    }
    
}
